# Trabalho de Padrões de Projeto

O código dos exercicios se encontram em `src/main/java/`. Dentro do pacote `br.uece.padroes` encontram-se vários 
sub-pacotes cada um contendo o código referente a cada questão.

O documento final gerado encontra-se em `src/docs/asciidoc/`. Dentro desta pasta encontram-se vários arquivos asciidoc 
(adoc) referentes a cada questão.

Para 'compilar' os arquivos adoc e gerar o html final, basta executar o comando `./gradlew -t asciidoctor`. E abrir em
um navegador o arquivo `build/asciidoc/html5/index.html`.

Uma documentação simples sobre a notação uml para gerar os diagramas pode ser encontrada no [link](http://plantuml.com/classes.html).