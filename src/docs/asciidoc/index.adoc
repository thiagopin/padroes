:last-update-label!:

= Trabalho de Padrões de Projeto
Thiago Oliveira <thiago@astux.com.br>; Marcos Borges <borges.marcos1@gmail.com>

include::q01.adoc[]

include::q02.adoc[]

include::q03.adoc[]

include::q04.adoc[]

include::q05.adoc[]

include::q06.adoc[]

include::q07.adoc[]

include::q08.adoc[]

include::q09.adoc[]

include::q10.adoc[]

include::q11.adoc[]

include::q12.adoc[]

include::q13.adoc[]

include::q14.adoc[]

include::q15.adoc[]

include::q16.adoc[]