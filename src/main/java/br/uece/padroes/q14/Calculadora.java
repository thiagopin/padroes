package br.uece.padroes.q14;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Arrays;
import java.util.LinkedList;

public class Calculadora {
    private LinkedList<String> ops = new LinkedList<>();

    public void adicionar(String op) {
        if (Arrays.asList("+", "-", "*", "/").contains(op) || isNumerico(op))
            ops.offer(op);
        else throw new IllegalArgumentException("Valor não é válido");
    }

    public void removerUltimo() {
        if (!ops.isEmpty()) {
            ops.remove(ops.size() - 1);
        }

    }

    public String montarExp() {
        String exp = "";
        for (String str : ops) {
            exp += str + ' ';
        }
        return exp.trim();
    }

    public void calcular() {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        String exp = montarExp();
        try {
            String result = engine.eval(exp).toString();
            System.out.println(String.format("%s = %s", exp, result));
        } catch (ScriptException e) {
            System.out.println(String.format("Não foi possivel avaliar a expressão: %s", exp));
        }
    }

    private boolean isNumerico(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
