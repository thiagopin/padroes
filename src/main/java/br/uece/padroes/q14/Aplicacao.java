package br.uece.padroes.q14;

public class Aplicacao {

    public static void main(String[] args) {
        Calculadora calc = new Calculadora();
        calc.adicionar("1");
        calc.adicionar("+");
        calc.adicionar("3");
        calc.adicionar("*");
        calc.adicionar("2");

        calc.calcular();

        calc.removerUltimo();
        calc.removerUltimo();
        calc.calcular();
    }
}
