package br.uece.padroes.q12;

public interface Pizzaiolo {

    String prepararPizza();
}
