package br.uece.padroes.q12;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class PizzaioloFactory {

    public static Pizzaiolo criarPizzaiolo(Date dia) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dia);
        int diaSemana = cal.get(Calendar.DAY_OF_WEEK);

        if (Arrays.asList(Calendar.MONDAY, Calendar.WEDNESDAY, Calendar.FRIDAY).contains(diaSemana))
            return new PizzaioloA();
        else if (Arrays.asList(Calendar.TUESDAY, Calendar.THURSDAY, Calendar.SATURDAY).contains(diaSemana))
            return new PizzaioloB();
        else return new SemPizzaiolo();
    }
}
