package br.uece.padroes.q12;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PizzariaBairro implements Pizzaria {

    @Override
    public String criarPizza(String dia) {
        try {
            Date data = new SimpleDateFormat("dd/MM/yyyy").parse(dia);
            Pizzaiolo pizzaiolo = PizzaioloFactory.criarPizzaiolo(data);
            return pizzaiolo.prepararPizza();
        } catch (ParseException e) {
            return "Dia inválido";
        }
    }
}
