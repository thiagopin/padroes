package br.uece.padroes.q12;

public class Aplicacao {

    public static void main(String[] args) {
        Pizzaria pizzaria = PizzariaFactory.criarPizzaria();

        String pizza = pizzaria.criarPizza("09/12/2015");
        System.out.println(pizza);

        pizza = pizzaria.criarPizza("10/12/2015");
        System.out.println(pizza);

        pizza = pizzaria.criarPizza("13/12/2015");
        System.out.println(pizza);
    }
}
