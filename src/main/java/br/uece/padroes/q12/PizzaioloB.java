package br.uece.padroes.q12;

public class PizzaioloB implements Pizzaiolo {
    
    @Override
    public String prepararPizza() {
        return "queijo + presunto + tomate";
    }
}
