package br.uece.padroes.q12;

public class PizzaioloA implements Pizzaiolo {

    @Override
    public String prepararPizza() {
        return "queijo + calabresa + tomate";
    }
}
