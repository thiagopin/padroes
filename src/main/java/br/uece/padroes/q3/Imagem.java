package br.uece.padroes.q3;

public abstract class Imagem {

	public abstract void carregar();
	public abstract void exibir();
	public abstract void fechar();

}
