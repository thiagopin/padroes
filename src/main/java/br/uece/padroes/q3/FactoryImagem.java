package br.uece.padroes.q3;

public class FactoryImagem {

	public static Visualizador visualizarPeloNome(String nome) {

		Visualizador v = new Visualizador();

		if (nome.equals("JPG")) {
			v = new VisualizadorJpg();
		}
		if (nome.equals("BMP")) {
			v = new VisualizadorBmp();
		}

		return v;
	}

}
