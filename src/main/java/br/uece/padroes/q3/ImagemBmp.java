package br.uece.padroes.q3;

public class ImagemBmp extends Imagem {
	
	String imagem = "BMP";
	
	public void carregar() {
		System.out.println("Imagem : " + this.imagem);
		System.out.println("Carregando imagem "+this.imagem+" ...");
	}

	public void exibir() {
		System.out.println("Exibindo imagem por 20 segundos");
	}

	public void fechar() {
		System.out.println("Fechando imagem");
	}
}
