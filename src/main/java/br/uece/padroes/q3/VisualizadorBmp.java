package br.uece.padroes.q3;

public class VisualizadorBmp extends Visualizador {
	
	@Override
	public void visualizar() {
		ImagemBmp img = new ImagemBmp();
		img.carregar();
		img.exibir();
		img.fechar();
	}
	
}
