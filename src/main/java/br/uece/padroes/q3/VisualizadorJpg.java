package br.uece.padroes.q3;

public class VisualizadorJpg extends Visualizador {
	
	@Override
	public void visualizar() {
		ImagemJpg img = new ImagemJpg();
		img.carregar();
		img.exibir();
		img.fechar();
	}
	
}
