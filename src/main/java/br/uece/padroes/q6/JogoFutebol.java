package br.uece.padroes.q6;

public class JogoFutebol extends Jogo implements JogoCommand {

    public JogoFutebol(String titulo) {
        super(titulo);
    }

    @Override
    public void executeA() {
        System.out.println(titulo + ": Correr");
    }

    @Override
    public void executeB() {
        System.out.println(titulo + ": Chutar");
    }
}
