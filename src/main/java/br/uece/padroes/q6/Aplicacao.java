package br.uece.padroes.q6;

public class Aplicacao {

    public static void main(String[] args) {
        Joystick joystick = new Joystick();

        joystick.setJogo(new JogoLuta("Street Fighter"));
        joystick.a();
        joystick.b();

        joystick.setJogo(new JogoCorrida("Daytona USA"));
        joystick.a();
        joystick.b();

        joystick.setJogo(new JogoFutebol("Super Star Soccer"));
        joystick.a();
        joystick.b();
    }
}
