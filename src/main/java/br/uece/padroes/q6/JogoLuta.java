package br.uece.padroes.q6;

public class JogoLuta extends Jogo implements JogoCommand {

    public JogoLuta(String titulo) {
        super(titulo);
    }

    @Override
    public void executeA() {
        System.out.println(titulo + ": Soco");
    }

    @Override
    public void executeB() {
        System.out.println(titulo + ": Chute");
    }
}
