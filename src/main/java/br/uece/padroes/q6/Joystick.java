package br.uece.padroes.q6;

public class Joystick {
    JogoCommand command;

    public void setJogo(JogoCommand command) {
        this.command = command;
    }

    public void a() {
        command.executeA();
    }

    public void b() {
        command.executeB();
    }
}
