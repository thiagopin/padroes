package br.uece.padroes.q6;

public class JogoCorrida extends Jogo implements JogoCommand {

    public JogoCorrida(String titulo) {
        super(titulo);
    }

    @Override
    public void executeA() {
        System.out.println(titulo + ": Acelerar");
    }

    @Override
    public void executeB() {
        System.out.println(titulo + ": Freiar");
    }
}
