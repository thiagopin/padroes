package br.uece.padroes.q8;

public enum ModoSaida {
    MAIUSCULA, MINUSCULA, DUPLICAR, INVERTER
}
