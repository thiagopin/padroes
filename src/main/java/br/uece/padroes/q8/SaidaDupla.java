package br.uece.padroes.q8;

public class SaidaDupla extends EntradaSaidaTemplate {

    @Override
    String transformar(String s) {
        return String.format("%s %s", s, s);
    }
}
