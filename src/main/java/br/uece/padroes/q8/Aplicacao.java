package br.uece.padroes.q8;

import br.uece.padroes.q6.JogoCorrida;
import br.uece.padroes.q6.JogoFutebol;
import br.uece.padroes.q6.JogoLuta;
import br.uece.padroes.q6.Joystick;

public class Aplicacao {

    public static void main(String[] args) {
        ExecucaoEntradaSaida es = new ExecucaoEntradaSaida(ModoSaida.MAIUSCULA);
        es.executar();

        es.setModoDeReproducao(ModoSaida.MINUSCULA);
        es.executar();

        es.setModoDeReproducao(ModoSaida.DUPLICAR);
        es.executar();

        es.setModoDeReproducao(ModoSaida.INVERTER);
        es.executar();
    }
}
