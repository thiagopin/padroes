package br.uece.padroes.q8;

public class SaidaInvertida extends EntradaSaidaTemplate {

    @Override
    String transformar(String s) {
        return new StringBuilder(s).reverse().toString();
    }
}
