package br.uece.padroes.q8;

import java.io.Console;
import java.util.Scanner;

public abstract class EntradaSaidaTemplate {
    abstract String transformar(String s);

    public void lerImprimir() {
        Scanner scan = new Scanner(System.in);
        String entrada = scan.nextLine();
        String saida = transformar(entrada);
        System.out.println(saida);
    }
}
