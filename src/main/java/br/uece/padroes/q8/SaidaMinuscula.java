package br.uece.padroes.q8;

public class SaidaMinuscula extends EntradaSaidaTemplate {

    @Override
    String transformar(String s) {
        return s.toLowerCase();
    }
}
