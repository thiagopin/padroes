package br.uece.padroes.q8;

public class ExecucaoEntradaSaida {

    private EntradaSaidaTemplate template;

    public ExecucaoEntradaSaida(ModoSaida modo) {
        setModoDeReproducao(modo);
    }

    public void setModoDeReproducao(ModoSaida modo) {
        template = null;
        switch (modo) {
            case MAIUSCULA:
                template = new SaidaMaiuscula();
                break;
            case MINUSCULA:
                template = new SaidaMinuscula();
                break;
            case DUPLICAR:
                template = new SaidaDupla();
                break;
            case INVERTER:
                template = new SaidaInvertida();
                break;
            default:
                break;
        }
    }

    public void executar() {
        template.lerImprimir();
    }
}
