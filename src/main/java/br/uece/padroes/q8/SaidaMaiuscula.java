package br.uece.padroes.q8;

public class SaidaMaiuscula extends EntradaSaidaTemplate {

    @Override
    String transformar(String s) {
        return s.toUpperCase();
    }
}
