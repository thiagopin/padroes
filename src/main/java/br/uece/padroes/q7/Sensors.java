package br.uece.padroes.q7;

public class Sensors {

	protected boolean temperatureSensor;
	protected boolean lightSensor;

	public Sensors(boolean temperatureSensor, boolean lightSensor) {

		this.temperatureSensor = temperatureSensor;
		this.lightSensor = lightSensor;
	
	}

}
