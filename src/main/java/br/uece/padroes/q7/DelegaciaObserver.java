package br.uece.padroes.q7;

public class DelegaciaObserver extends Observer {

	public DelegaciaObserver(Alarm alarm) {
		super(alarm);
	}

	public void update() {
		if (alarm.getState().lightSensor) {
			System.out.println("Sensor de Luz foi violado  na Delegacia");
		}
		if (alarm.getState().temperatureSensor) {
			System.out.println("Sensor de temperatura foi violado na Delegacia");
		}
	}
}
