package br.uece.padroes.q7;

public class SeguroObserver extends Observer {
	
	public SeguroObserver(Alarm alarm) {
		super(alarm);
	}
	public void update() {
		if(alarm.getState().lightSensor) {
			System.out.println("Sensor de Luz foi violado  na Cia de Seguros");
		}
		if(alarm.getState().temperatureSensor) {
			System.out.println("Sensor de temperatura foi violado no Cia de Seguros");
		}
	}
	
}
