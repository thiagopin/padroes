package br.uece.padroes.q7;

public class Aplicacao {
	
	public static void main(String[] args) {
		Alarm alarm = new Alarm();
		alarm.attach(new SeguroObserver(alarm));
		alarm.attach(new DelegaciaObserver(alarm));
		alarm.setState(new Sensors(false, false));
		alarm.setState(new Sensors(true, false));
		alarm.setState(new Sensors(false, true));
	}

}
