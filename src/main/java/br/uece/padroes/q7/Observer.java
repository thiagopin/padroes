package br.uece.padroes.q7;

public abstract class Observer {
	
	public Alarm alarm;
	public Observer(Alarm alarm) {
		this.alarm = alarm;
	}
	public abstract void update();
	
}
