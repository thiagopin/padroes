package br.uece.padroes.q7;

import java.util.ArrayList;

public class Alarm {
	protected ArrayList<Observer> observers;
	protected Sensors sensors;

	public Alarm() {
		observers = new ArrayList<Observer>();
	}

	public void attach(Observer observer) {
		observers.add(observer);
	}

	public void detach(int indice) {
		observers.remove(indice);
	}

	public Sensors getState() {
		return sensors;
	}

	public void setState(Sensors sensors) {
		this.sensors = sensors;
		this.notifyObservers();
	}
	public void notifyObservers() {
		for (Observer observer : observers) {
			observer.update();
		}
	}
	

}
