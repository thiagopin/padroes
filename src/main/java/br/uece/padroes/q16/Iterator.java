package br.uece.padroes.q16;

public interface Iterator<T> {
    T proximo();

    T remover();

    void colocarNoFinal(T element);

    boolean estaVazia();
}
