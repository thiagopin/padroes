package br.uece.padroes.q16;

public class Jogo {
    Iterator<Integer> jogadorA;
    Iterator<Integer> jogadorB;

    public Jogo(Iterator<Integer> jogadorA, Iterator<Integer> jogadorB) {
        this.jogadorA = jogadorA;
        this.jogadorB = jogadorB;
    }

    public void iniciarPartida() {
        while (!jogadorA.estaVazia() && !jogadorB.estaVazia()) {
            imprimirSituacao();

            if (jogadorA.proximo() < jogadorB.proximo()) {
                jogadorB.colocarNoFinal(jogadorA.remover());
                jogadorB.colocarNoFinal(jogadorB.remover());
            } else if (jogadorB.proximo() < jogadorA.proximo()) {
                jogadorA.colocarNoFinal(jogadorB.remover());
                jogadorA.colocarNoFinal(jogadorA.remover());
            } else {
                jogadorA.colocarNoFinal(jogadorA.remover());
                jogadorB.colocarNoFinal(jogadorB.remover());
            }

        }

        System.out.println("Fim de jogo!! Vencedor é o jogador " + (jogadorA.estaVazia() ? "B" : "A"));
    }

    public void imprimirSituacao() {
        System.out.println(jogadorA);
        System.out.println(jogadorB);
        System.out.println("=====================================================================================");
    }
}
