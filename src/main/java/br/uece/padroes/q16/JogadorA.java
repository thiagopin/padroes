package br.uece.padroes.q16;

import java.util.ArrayList;
import java.util.List;

public class JogadorA implements Iterator<Integer> {
    private List<Integer> cartas = new ArrayList<Integer>();

    public JogadorA(List<Integer> cartas) {
        this.cartas.addAll(cartas);
    }

    @Override
    public Integer proximo() {
        return cartas.get(0);
    }

    @Override
    public Integer remover() {
        return cartas.remove(0);
    }

    @Override
    public void colocarNoFinal(Integer carta) {
        cartas.add(carta);
    }

    @Override
    public boolean estaVazia() {
        return cartas.isEmpty();
    }

    @Override
    public String toString() {
        return "A => Carta: " + (estaVazia() ? "-" : proximo()) + ", Deck: " + (estaVazia() ? "[]" : cartas.subList(1, cartas.size()));
    }
}
