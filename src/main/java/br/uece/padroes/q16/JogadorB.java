package br.uece.padroes.q16;

import java.util.*;

public class JogadorB implements Iterator<Integer> {
    private LinkedList<Integer> cartas = new LinkedList<Integer>();

    public JogadorB(List<Integer> cartas) {
        this.cartas.addAll(cartas);
    }

    @Override
    public Integer proximo() {
        return cartas.peek();
    }

    @Override
    public Integer remover() {
        return cartas.poll();
    }

    @Override
    public void colocarNoFinal(Integer carta) {
        cartas.offer(carta);
    }

    @Override
    public boolean estaVazia() {
        return cartas.isEmpty();
    }

    @Override
    public String toString() {
        return "B => Carta: " + (estaVazia() ? "-" : proximo()) + ", Deck: " + (estaVazia() ? "[]" : cartas.subList(1, cartas.size()));
    }
}
