package br.uece.padroes.q16;

import java.util.Arrays;

public class Aplicacao {

    public static void main(String[] args) {
        Iterator<Integer> jogadorA = new JogadorA(Arrays.asList(1, 2, 3, 4, 1, 2, 3));
        Iterator<Integer> jogadorB = new JogadorB(Arrays.asList(5, 1, 4, 5, 2, 3, 4));

        new Jogo(jogadorA, jogadorB).iniciarPartida();
    }
}
