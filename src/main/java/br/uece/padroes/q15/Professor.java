package br.uece.padroes.q15;

public class Professor extends Colleague {

	public Professor(Mediator mediator) {
		super(mediator);
	}
	
	public void enviarTrabalho(String mensagem) {
		this.getMediator().enviarTrabalhoAlunos(mensagem, this);
	}
	
	@Override
	public void receberTrabalho(String mensagem, Colleague colleague) {
		// TODO Auto-generated method stub
		System.out.println("Professor(a) "+this.getNome()+" recebeu do aluno(a) "+colleague.getNome()+" o trabalho : "+mensagem);
	}
	public void receberMensagem(String mensagem, Colleague colleague) {
		// TODO Auto-generated method stub
		System.out.println("Professor(a) "+this.getNome()+" recebeu do aluno(a) "+colleague.getNome()+" a mensagem : "+mensagem);
	}
}
