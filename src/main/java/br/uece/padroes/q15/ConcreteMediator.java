package br.uece.padroes.q15;

import java.util.ArrayList;

public class ConcreteMediator implements Mediator {

	public ArrayList<Colleague> colleagues;

	public ConcreteMediator() {
		colleagues = new ArrayList<Colleague>();
	}

	public ArrayList<Colleague> getContatos() {
		return colleagues;
	}

	public void adicionarColleague(Colleague colleague) {
		colleagues.add(colleague);
	}

	public void enviarTrabalho(String mensagem, Colleague target, Colleague source) {
		target.receberTrabalho(mensagem,source);
	}

	public void enviarTrabalhoAlunos(String mensagem, Colleague colleague) {
		for (Colleague contato : colleagues) {
			if (contato != colleague && contato instanceof Aluno)
				contato.receberTrabalho(mensagem, colleague);
		}
	}
	public void enviarMensagem(String mensagem, Colleague colleague) {
		for (Colleague contato : colleagues) {
			if (contato != colleague && contato instanceof Aluno)
				contato.receberMensagem(mensagem, colleague);
		}
	}
}
