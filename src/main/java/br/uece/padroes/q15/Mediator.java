package br.uece.padroes.q15;

public interface Mediator {

	public void enviarTrabalho(String mensagem, Colleague target, Colleague source); //Aluno - Professor
	public void enviarTrabalhoAlunos(String mensagem, Colleague colleague); //Professor - Alunos
	public void enviarMensagem(String mensagem, Colleague colleague); //Mensagens Alunos
	
}
