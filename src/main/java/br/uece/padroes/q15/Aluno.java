package br.uece.padroes.q15;

public class Aluno extends Colleague {
	
	public Aluno(Mediator mediator) {
		super(mediator);
	}
	
	public void enviarTrabalhoProfessor(String mensagem, Colleague colleague) {
		this.getMediator().enviarTrabalho(mensagem, colleague, this);
	}
	
	public void enviarMensagem(String mensagem) {
		this.getMediator().enviarMensagem(mensagem, this);
	}
	
	@Override
	public void receberTrabalho(String mensagem, Colleague colleague) {
		// TODO Auto-generated method stub
		System.out.println("Aluno(a) "+this.getNome()+" recebeu o trabalho do professor(a) "+colleague.getNome()+" : "+ mensagem);
	}
	
	public void receberMensagem(String mensagem, Colleague colleague) {
		// TODO Auto-generated method stub
		System.out.println("Aluno(a) "+this.getNome()+" recebeu a mensagem do aluno(a) "+colleague.getNome()+" : "+ mensagem);
	}
	
	
}
