package br.uece.padroes.q15;

public class Aplicacao {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConcreteMediator mediator = new ConcreteMediator();
		
		Aluno aluno = new Aluno(mediator);
		aluno.setNome("Marcos");
		Aluno aluno2 = new Aluno(mediator);
		aluno2.setNome("Thiago");
		Aluno aluno3 = new Aluno(mediator);
		aluno3.setNome("Anderson");
		Aluno aluno4 = new Aluno(mediator);
		aluno4.setNome("Marcelo");
		
		Professor professor = new Professor(mediator);
		professor.setNome("Paulo");
		Professor professor2 = new Professor(mediator);
		professor2.setNome("Valdisio");
		
		
		mediator.adicionarColleague(aluno);
		mediator.adicionarColleague(aluno2);
		mediator.adicionarColleague(aluno3);
		mediator.adicionarColleague(aluno4);
		mediator.adicionarColleague(professor);
		mediator.adicionarColleague(professor2);
		
		aluno.enviarTrabalhoProfessor("Trabalho de padroes", professor2); //Envia para um professor
		professor.enviarTrabalho("Trabalho GOF para segunda");// Envia para todos os alunos
		aluno.enviarMensagem("E aí galera");
		aluno2.enviarMensagem("Fala aí cara!");
		
	}

}
