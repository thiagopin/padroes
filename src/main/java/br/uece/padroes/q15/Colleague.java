package br.uece.padroes.q15;

public abstract class Colleague {

	protected Mediator mediator;
	protected String nome;

	public Colleague(Mediator mediator) {
		this.mediator = mediator;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Mediator getMediator() {
		return this.mediator;
	}

	public abstract void receberTrabalho(String mensagem, Colleague colleague);

	public abstract void receberMensagem(String mensagem, Colleague colleague);

}
