package br.uece.padroes.q4;

public class ComCastanha extends YogurtDecorator {

    public ComCastanha(Yogurt umYogurt) {
        super(umYogurt);
        nome = "Castanha";
        preco = 1.5;
    }
}
