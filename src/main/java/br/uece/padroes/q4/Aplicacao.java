package br.uece.padroes.q4;

public class Aplicacao {

    public static void main(String[] args) {
        Yogurt meuYogurt = new YogurtPuro();
        System.out.println(meuYogurt.getNome() + " = "
                + meuYogurt.getPreco());

        meuYogurt = new ComCastanha(meuYogurt);
        System.out.println(meuYogurt.getNome() + " = "
                + meuYogurt.getPreco());

        meuYogurt = new ComChocolate(meuYogurt);
        System.out.println(meuYogurt.getNome() + " = "
                + meuYogurt.getPreco());
    }
}
