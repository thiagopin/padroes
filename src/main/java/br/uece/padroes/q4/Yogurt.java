package br.uece.padroes.q4;

public abstract class Yogurt {
    String nome;
    double preco;

    public String getNome() {
        return nome;
    }

    public double getPreco() {
        return preco;
    }
}
