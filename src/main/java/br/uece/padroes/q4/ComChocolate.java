package br.uece.padroes.q4;

public class ComChocolate extends YogurtDecorator {

    public ComChocolate(Yogurt umYogurt) {
        super(umYogurt);
        nome = "Chocolate";
        preco = 2.0;
    }
}
