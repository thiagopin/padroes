package br.uece.padroes.q4;

public abstract class YogurtDecorator extends Yogurt {
    Yogurt yogurt;

    public YogurtDecorator(Yogurt umYogurt) {
        this.yogurt = umYogurt;
    }

    @Override
    public String getNome() {
        return yogurt.getNome() + " + " + nome;
    }

    public double getPreco() {
        return yogurt.getPreco() + preco;
    }
}