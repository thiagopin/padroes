package br.uece.padroes.q2;

public class Aplicacao {
    public static void main(String[] args) {
        Logger log1 = new LoggerEventViewer();
        log1.setAtivo(true);
        log1.log("PRIMEIRA MENSAGEM DE LOG");

        Logger log2 = new LoggerFile();
        log2.setAtivo(true);
        log2.log("SEGUNDA MENSAGEM DE LOG");

        Logger log3 = new LoggerDatabase();
        log3.setAtivo(true);
        log3.log("TERCEIRA MENSAGEM DE LOG");
    }
}
