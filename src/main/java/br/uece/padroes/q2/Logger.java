package br.uece.padroes.q2;

public abstract class Logger {

    private boolean ativo = false;

    public boolean isAtivo() {
        return this.ativo;
    }

    public void setAtivo(boolean b) {
        this.ativo = b;
    }

    public void log(String s) {
        if (this.ativo)
            saida(s);
    }

    public abstract void saida(String s);
}
