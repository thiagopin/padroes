package br.uece.padroes.q10;

public class ListaDesordenada extends Lista {

    public ListaDesordenada(ImpressaoLista impressaoLista) {
        super(impressaoLista);
    }

    @Override
    public void adicionar(String str) {
        this.itens.add(str);
    }
}
