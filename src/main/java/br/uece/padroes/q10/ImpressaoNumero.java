package br.uece.padroes.q10;

import java.util.List;

public class ImpressaoNumero implements ImpressaoLista {

    public void imprimeItens(List<String> itens) {
        int counter = 1;
        for (String str : itens) {
            System.out.println(String.format("%d. %s", counter++, str));
        }
    }
}
