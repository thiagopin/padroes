package br.uece.padroes.q10;

public class Aplicacao {

    public static void main(String[] args) {
        Lista lista = new ListaDesordenada(new ImpressaoNumero());
        lista.adicionar("Ameaça Fantasma");
        lista.adicionar("Guerra dos Clones");
        lista.adicionar("A Vingança dos Sith");

        lista.imprimir();
        System.out.println();


        lista = new ListaOrdenada(new ImpressaoMarcador());
        lista.adicionar("Uma Nova Esperança");
        lista.adicionar("O Império Contra Ataca");
        lista.adicionar("O Retorno de Jedi");

        lista.imprimir();
        System.out.println();

        lista = new ListaOrdenada(new ImpressaoLetra());
        lista.adicionar("Luke");
        lista.adicionar("Anakin");
        lista.adicionar("Hans");

        lista.imprimir();
    }
}
