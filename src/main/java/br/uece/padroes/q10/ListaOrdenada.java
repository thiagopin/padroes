package br.uece.padroes.q10;

import java.util.Collections;

public class ListaOrdenada extends Lista {

    public ListaOrdenada(ImpressaoLista impressaoLista) {
        super(impressaoLista);
    }

    @Override
    public void adicionar(String str) {
        this.itens.add(str);
        Collections.sort(this.itens);
    }
}
