package br.uece.padroes.q10;

import java.util.List;

public class ImpressaoLetra implements ImpressaoLista {

    public void imprimeItens(List<String> itens) {
        char counter = 'a';
        for (String str : itens) {
            System.out.println(String.format("%c. %s", counter++, str));
        }
    }
}
