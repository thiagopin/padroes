package br.uece.padroes.q10;

import java.util.ArrayList;
import java.util.List;

public abstract class Lista {
    protected List<String> itens = new ArrayList<>();
    private ImpressaoLista impressaoLista;

    public Lista(ImpressaoLista impressaoLista) {
        this.impressaoLista = impressaoLista;
    }

    public abstract void adicionar(String str);

    public void imprimir() {
        impressaoLista.imprimeItens(itens);
    }
}
