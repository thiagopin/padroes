package br.uece.padroes.q10;

import java.util.List;

public class ImpressaoMarcador implements ImpressaoLista {

    public void imprimeItens(List<String> itens) {
        for (String str : itens) {
            System.out.println(String.format("* %s", str));
        }
    }
}
