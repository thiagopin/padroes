package br.uece.padroes.q10;

import java.util.List;

public interface ImpressaoLista {

    void imprimeItens(List<String> itens);
}
