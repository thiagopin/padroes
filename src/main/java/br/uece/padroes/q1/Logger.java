package br.uece.padroes.q1;

public class Logger {

    private static Logger logger;

    static {
        logger = new Logger();
    }

    private boolean ativo = false;

    private Logger() {
    }

    public static Logger getInstance() {
        return logger;
    }

    public boolean isAtivo() {
        return this.ativo;
    }

    public void setAtivo(boolean b) {
        this.ativo = b;
    }

    public void log(String s) {
        if (this.ativo)
            System.out.println("LOG :: " + s);
    }
}
