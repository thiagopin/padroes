package br.uece.padroes.q9;

import java.util.ArrayList;

public class Instituicao extends Participante {

	ArrayList<Participante> membros = new ArrayList<Participante>();

	public Instituicao(String nome) {
		this.nome = nome;
	}

	public void adicionar(Participante participante) {
		this.membros.add(participante);
	}

	public void remover(Participante participante) {
		this.membros.remove(participante);
	}

	public void imprimirMembros() {
		for (Participante membro : membros) {
			System.out.println("Membro: "+ membro.getNome());
		}
	}


}
