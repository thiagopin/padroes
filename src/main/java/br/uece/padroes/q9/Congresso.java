package br.uece.padroes.q9;

import java.util.ArrayList;

public class Congresso {

	public String nome;
	public int totalAssentos;
	public ArrayList<Participante> participantes;
	
	public Congresso(String nome) {
		this.nome = nome;
		this.participantes = new ArrayList<Participante>();
	}
	
	public int getTotalAssentos() {
		return totalAssentos;
	}
	public void setTotalAssentos(int totalAssentos) {
		this.totalAssentos = totalAssentos;
	}
	public int getTotalParticipantes() {
		return this.participantes.size();
	}
	public void adicionarParticipante(Participante p) {
		if(p instanceof Instituicao) {
			this.adicionarMembrosInstituicao((Instituicao) p);
		}
		else {
			this.adicionar(p);
		}
	}
	public void adicionarMembrosInstituicao(Instituicao i) {
		for (Participante participante : i.membros) {
			this.adicionar(participante);
		}
	}
	private void adicionar(Participante p) {
		if(this.getTotalAssentos() > this.getTotalParticipantes() && !this.participantes.contains(p)) {
			this.participantes.add(p);
		}
		else {
			System.err.println("Participante: "+p.getNome()+" não pode ser adicionado! Não há assentos ou já foi cadastrado!!");
		}
	}
	
	public void imprimeParticipantes() {
		for (Participante participante : participantes) {
			System.out.println("Participante inscritos: "+ participante.getNome());
		}
	}
	
	
}
