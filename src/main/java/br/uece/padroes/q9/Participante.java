package br.uece.padroes.q9;

public class Participante {

	protected String nome;

	public String getNome() {
		return nome;
	}

	public int getAssento() {
		return 0;
	}

	public void adicionar(Participante participante) throws Exception {
		throw new Exception("O participante " + participante.getNome() + " não pode ser adicionado");
	}

	public void remover(Participante participante) throws Exception {
		throw new Exception("O participante " + participante.getNome() + " não pode ser removido");
	}

}
