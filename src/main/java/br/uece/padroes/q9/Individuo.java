package br.uece.padroes.q9;

public class Individuo extends Participante {

	private int assento;

	public Individuo(String nome, int assento) {
		
		this.nome  = nome;
		this.assento  = assento;

	}

	public int getAssento() {
		return assento;
	}

}
