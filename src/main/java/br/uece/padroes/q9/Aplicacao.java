package br.uece.padroes.q9;

public class Aplicacao {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Congresso c = new Congresso("CBSOFT");
		c.setTotalAssentos(2);
		
		Individuo i1 = new Individuo("Marcos",1);
		Individuo i2 = new Individuo("Marcia",2);
		Individuo i3 = new Individuo("Jesus",3);
		Individuo i4 = new Individuo("Salvador",4);
		
		Instituicao instituicao = new Instituicao("UECE");
		instituicao.adicionar(i1);
		instituicao.adicionar(i2);
		instituicao.remover(i2);
		
		c.adicionarParticipante(instituicao);
		c.adicionarParticipante(i3);
		c.adicionarParticipante(i4);
		
		//c.adicionar(i1);
		//c.adicionar(i2);
		//c.adicionar(i3);
		System.out.println(c.getTotalParticipantes());
		c.imprimeParticipantes();
	}

}
