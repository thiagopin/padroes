package br.uece.padroes.q11;

public class Aplicacao {
	public static void main(String[] args) {
		Cliente cliente = new Cliente(new Adapter());		
		cliente.executar();
	}
}
