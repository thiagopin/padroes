package br.uece.padroes.q11;

public interface SomadorEsperado {
	int somaVetor(int[] vetor);
}
