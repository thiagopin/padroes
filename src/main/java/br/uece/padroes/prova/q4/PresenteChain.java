package br.uece.padroes.prova.q4;

import java.util.ArrayList;

public abstract class PresenteChain {

    protected PresenteChain next;
    protected Presentes presente;
    protected ArrayList<String> nomes;

    public PresenteChain(Presentes presente) {
        this.next = null;
        this.presente = presente;
    }

    public void setNext(PresenteChain presentechain) {
        if (next == null) {
            next = presentechain;
        } else {
            next.setNext(presentechain);
        }
    }

    public void entregarPresentes(String nomeCrianca) {
        if (this.entregarPresente(nomeCrianca)) {
        } else if (next != null) {
            next.entregarPresentes(nomeCrianca);
        }
    }

    public abstract boolean entregarPresente(String nomeDaCrianca);
}
