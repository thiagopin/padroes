package br.uece.padroes.prova.q4;

import java.util.ArrayList;
import java.util.Arrays;

public class VideoGame extends PresenteChain {

    public VideoGame() {
        super(Presentes.videoGame);
        this.nomes = new ArrayList<String>(Arrays.asList("italo", "rian"));
    }

    @Override
    public boolean entregarPresente(String nomeDaCrianca) {
        for (String nome : this.nomes) {
            if (nome.equals(nomeDaCrianca)) {
                System.out.println("Video Game entregue a " + nome);
                this.nomes.remove(nomeDaCrianca.indexOf(nome)); //remove a crianca da lista
                return true;
            }
        }
        return false;
    }

}
