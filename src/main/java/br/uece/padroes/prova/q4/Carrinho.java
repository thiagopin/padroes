package br.uece.padroes.prova.q4;

import java.util.ArrayList;
import java.util.Arrays;

public class Carrinho extends PresenteChain {

    public Carrinho() {
        super(Presentes.carrinho);
        this.nomes = new ArrayList<String>(Arrays.asList("joao vitor", "tiago"));
    }

    @Override
    public boolean entregarPresente(String nomeDaCrianca) {
        for (String nome : this.nomes) {
            if (nome.equals(nomeDaCrianca)) {
                System.out.println("Carrinho entregue a " + nome);
                this.nomes.remove(nomeDaCrianca.indexOf(nome)); //remove a crianca da lista
                return true;
            }
        }
        return false;
    }

}
