package br.uece.padroes.prova.q4;

import java.util.ArrayList;
import java.util.Arrays;

public class JogoTabuleiro extends PresenteChain {

    public JogoTabuleiro() {
        super(Presentes.jogoTabuleiro);
        this.nomes = new ArrayList<String>(Arrays.asList("atus", "matheus"));
    }

    @Override
    public boolean entregarPresente(String nomeDaCrianca) {
        for (String nome : this.nomes) {
            if (nome.equals(nomeDaCrianca)) {
                System.out.println("Jogo Tabuleiro entregue a " + nome);
                this.nomes.remove(nomeDaCrianca.indexOf(nome)); //remove a crianca da lista
                return true;
            }
        }
        return false;
    }

}
