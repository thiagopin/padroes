package br.uece.padroes.prova.q4;

import java.util.ArrayList;
import java.util.Arrays;

public class Boneca extends PresenteChain {

    public Boneca() {
        super(Presentes.boneca);
        this.nomes = new ArrayList<String>(Arrays.asList("ana beatriz", "maria cecilia"));
    }

    @Override
    public boolean entregarPresente(String nomeDaCrianca) {
        for (String nome : this.nomes) {
            if (nome.equals(nomeDaCrianca)) {
                System.out.println("Boneca entregue a " + nome);
                this.nomes.remove(nomeDaCrianca.indexOf(nome)); //remove a crianca da lista
                return true;
            }
        }
        return false;
    }

}
