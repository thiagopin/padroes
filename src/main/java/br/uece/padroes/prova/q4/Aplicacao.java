package br.uece.padroes.prova.q4;

public class Aplicacao {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        String nomesDasCriancas[] = {"atus", "ana beatriz", "tiago", "italo"};
        PresenteChain presente = new Carrinho();
        presente.setNext(new Boneca());
        presente.setNext(new JogoTabuleiro());
        presente.setNext(new VideoGame());

        for (String nomes : nomesDasCriancas) {
            presente.entregarPresentes(nomes);
        }
    }

}
