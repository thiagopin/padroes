package br.uece.padroes.prova.q9;

public class Consulta {

	private String select, from, where, orderBy;

	public Consulta(String select, String from, String where, String orderBy) {
		this.select = select;
		this.from = from;
		this.where = where;
		this.orderBy = orderBy;
	}

	public String getSelect() {
		return select;
	}

	public String getFrom() {
		return from;
	}

	public String getWhere() {
		return where;
	}

	public String getOrderBy() {
		return orderBy;
	}

}
