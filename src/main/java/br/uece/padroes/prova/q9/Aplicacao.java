package br.uece.padroes.prova.q9;

public class Aplicacao {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Director director = new Director(new SqlBuilder());
        director.montarClausula(new Consulta("c.nome, c.descricao, p.preco", "Cliente C, Consulta P", "c.cidade='fortaleza'", "p.preco"));
        System.out.println(director.toString());
    }

}
