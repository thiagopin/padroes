package br.uece.padroes.prova.q9;

public abstract class Builder {

    protected Consulta consulta;
    protected String clausula = "";

    public abstract void adicionarClausulaSelect();

    public abstract void adicionarClausulaFrom();

    public abstract void adicionarClausulaWhere();

    public abstract void adicionarClausulaOrderBy();

    public void setProduto(Consulta consulta) {
        this.consulta = consulta;
    }

    public Consulta getConsulta() {
        return this.consulta;
    }

}
