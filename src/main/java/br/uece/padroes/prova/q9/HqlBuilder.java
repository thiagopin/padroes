package br.uece.padroes.prova.q9;

public class HqlBuilder extends Builder {

	public void adicionarClausulaSelect() {
		this.clausula += "select " + this.getConsulta().getSelect();
	}

	public void adicionarClausulaFrom() {
		this.clausula += " from " + this.getConsulta().getFrom();
		
	}

	public void adicionarClausulaWhere() {
		this.clausula += " where " + this.getConsulta().getWhere();
	}

	public void adicionarClausulaOrderBy() {
		this.clausula += " order by " + this.getConsulta().getOrderBy();
	}

	public String toString() {
		return "Consulta em HQL: " + this.clausula;
	}

}
