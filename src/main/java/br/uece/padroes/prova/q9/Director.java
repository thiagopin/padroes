package br.uece.padroes.prova.q9;

public class Director {
	
	protected Builder builder;
	
	public Director(Builder builder) {
		this.builder = builder;
	}
	public Builder getBuilder() {
		return this.builder;
	}
	public void montarClausula(Consulta consulta) {
		
		this.getBuilder().setProduto(consulta);
		this.getBuilder().adicionarClausulaSelect();
		this.getBuilder().adicionarClausulaFrom();
		this.getBuilder().adicionarClausulaWhere();
		this.getBuilder().adicionarClausulaOrderBy();
	}
	public String toString() {
		return this.getBuilder().toString();
	}
}
