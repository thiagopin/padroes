package br.uece.padroes.prova.q10;

/**
 * Created by thiago on 10/01/16.
 */
public class Laranja extends Refrigerante {

    public Laranja(Recipiente recipiente) {
        super(recipiente);
    }

    @Override
    public String sabor() {
        return "Laranja";
    }
}
