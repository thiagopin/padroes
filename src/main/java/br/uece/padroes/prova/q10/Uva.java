package br.uece.padroes.prova.q10;

/**
 * Created by thiago on 10/01/16.
 */
public class Uva extends Refrigerante {

    public Uva(Recipiente recipiente) {
        super(recipiente);
    }

    @Override
    public String sabor() {
        return "Uva";
    }
}
