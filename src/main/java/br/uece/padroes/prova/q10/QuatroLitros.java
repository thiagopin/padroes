package br.uece.padroes.prova.q10;

/**
 * Created by thiago on 10/01/16.
 */
public class QuatroLitros extends Recipiente {
    @Override
    public String tamanho() {
        return "4L";
    }
}
