package br.uece.padroes.prova.q10;

/**
 * Created by thiago on 10/01/16.
 */
public class Aplicacao {

    public static void main(String[] args) {
        Refrigerante ref = new Uva(new Lata());
        System.out.println(ref.descricao());

        ref = new Laranja(new DoisLitros());
        System.out.println(ref.descricao());

        ref = new Laranja(new QuatroLitros());
        System.out.println(ref.descricao());
    }
}
