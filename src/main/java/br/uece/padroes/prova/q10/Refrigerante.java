package br.uece.padroes.prova.q10;

/**
 * Created by thiago on 10/01/16.
 */
public abstract class Refrigerante {

    protected Recipiente recipiente;

    public Refrigerante(Recipiente recipiente) {
        this.recipiente = recipiente;
    }

    public abstract String sabor();

    public String descricao() {
        return String.format("Refrigerante de %s em %s", this.sabor(), recipiente.tamanho());
    }
}
