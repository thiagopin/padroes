package br.uece.padroes.q5;

public class Aplicacao {
	public static void main(String[] args) {
		
		SlotChain slot = new Slot1();
		slot.setNext(new Slot5());
		slot.setNext(new Slot10());
		slot.setNext(new Slot25());
		slot.setNext(new Slot50());
			
		try {
			
			Moeda moedas[] = { new Moeda(50),new Moeda(5), new Moeda(50), new Moeda(50), new Moeda(25),new Moeda(50),new Moeda(50) };
			
			for (Moeda moeda : moedas) {
				slot.receberMoeda(moeda);	
			}
			
			double valorEmReais = slot.getValorTotalMoedas()/100;
			double valorSalgadinho = 2.50, valorRefrigerante = 1.00;
			
			System.out.printf("Total em dinheiro ---> %.2f\n",valorEmReais);
			System.out.println("Opções de compra");
			if(valorEmReais>=valorSalgadinho) {
				System.out.printf("Salgadinho valor ---> %.2f\n",valorSalgadinho);
				System.out.printf("Troco %.2f\n", (valorEmReais-valorSalgadinho));
			}
			if(valorEmReais>=valorRefrigerante) {
				System.out.printf("Refrigerante valor ---> %.2f\n", valorRefrigerante);
				System.out.printf("Troco %.2f", (valorEmReais-valorRefrigerante));
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}
