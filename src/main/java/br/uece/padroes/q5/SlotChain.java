package br.uece.padroes.q5;

public abstract class SlotChain {

	protected SlotChain next;
	protected IDSlots idSlot;
	protected int valorAceitoSlot;
	protected int valorTotalMoedas = 0;

	public SlotChain(IDSlots idSlot) {
		next = null;
		this.idSlot = idSlot;
	}

	public void setNext(SlotChain slot) {
		if (next == null) {
			next = slot;
		} else {
			next.setNext(slot);
		}
	}

	public void receberMoeda(Moeda moeda) {
		if (this.podeReceberMoeda(moeda)) {
			this.contabilizarMoeda(moeda);
			
		} else if(next!=null) {
			next.receberMoeda(moeda);
		}
	}

	private boolean podeReceberMoeda(Moeda moeda) {
		return (valorAceitoSlot == moeda.getValorMoeda()) ? true : false;
	}

	protected void contabilizarMoeda(Moeda moeda) {
		
		this.valorTotalMoedas += moeda.getValorMoeda();
	}

	protected double getValorTotalMoedas() {
		
		return valorTotalMoedas + ((next == null) ? 0 : next.getValorTotalMoedas());
	}
	
}
