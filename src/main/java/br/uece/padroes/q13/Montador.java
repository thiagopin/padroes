package br.uece.padroes.q13;

public abstract class Montador {
	 
    protected Produto produto;
    protected Pedido pedido;
 
    public abstract void adicionarSanduiche();
 
    public abstract void adcionarBatata();
 
    public abstract void adicionarBrinquedo();
 
    public abstract void adicionarRefrigerante();
    
    public Pedido getPedido() {
		return this.pedido;
	}
    public Produto getProduto() {
		return this.produto;
	}
 
}
