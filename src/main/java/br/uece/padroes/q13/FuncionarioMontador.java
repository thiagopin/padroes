package br.uece.padroes.q13;

public class FuncionarioMontador extends Montador {
	
	public FuncionarioMontador() {
		this.pedido = new Pedido();
	}
	public void adicionarSanduiche() {
		this.getPedido().adicionarDentroDaCaixa(this.getProduto().getSanduiche());
	}

	public void adcionarBatata() {
		this.getPedido().adicionarDentroDaCaixa(this.getProduto().getBatata());
	}

	public void adicionarBrinquedo() {
		this.getPedido().adicionarDentroDaCaixa(this.getProduto().getBrinquedo());
	}

	public void adicionarRefrigerante() {
		this.getPedido().adicionarForaDaCaixa(this.getProduto().getRefrigerante());
	}
	public Pedido getPedido() {
		return this.pedido;
	}

}
