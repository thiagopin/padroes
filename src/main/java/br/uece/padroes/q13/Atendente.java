package br.uece.padroes.q13;

public class Atendente {

	protected Montador montador;

	public Atendente(Montador montador) {
		this.montador = montador;
	}

	public void receberPedido(Produto produto) {
		this.getMontador().produto = produto;
		this.getMontador().adicionarSanduiche();
		this.getMontador().adcionarBatata();
		this.getMontador().adicionarBrinquedo();
		this.getMontador().adicionarRefrigerante();
	}

	public Montador getMontador() {
		return this.montador;
	}

	public void imprimirPedido() {
		System.out.println(this.getMontador().getPedido().toString());
	}

}
