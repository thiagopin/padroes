package br.uece.padroes.q13;

public class Aplicacao {
	public static void main(String[] args) {
		
		Atendente atendente = new Atendente(new FuncionarioMontador());
		atendente.receberPedido(new Produto("sanduiche", "batata", "brinquedo", "refrigerante"));
		atendente.imprimirPedido();
		
		atendente = new Atendente(new FuncionarioMontador());
		atendente.receberPedido(new Produto("hamburger", "batata", "brinquedo", "coca-cola"));
		atendente.imprimirPedido();
	}
}
